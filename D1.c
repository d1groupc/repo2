/*
 * D1.c
 *
 *  Created on: 15 Jan 2013
 *      Author: ewk1g12 & Steve Gunn!
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NO_TESTS 10
int BSR_TDI[192];
int BSR_TDO[192];
int BSR_MASK[192];

int input_bit[32] = {183, 177, 168, 153, 150, 147, 141, 3, 135, 120, -1, 108, 105, 102, 96, 0, 93, 87, 84, 81, 72, 69, 54, 189, 48, 42, -1, 36, 27, 12, 6, 186 };
int control_bit[32] = {184, 178, 169, 154, 151, 148, 142, 4, 136, 121, -1, 109, 106, 103, 97, 1, 94, 88, 85, 82, 73, 70, 55, 190, 49, 43, -1, 37, 28, 13, 7, 187 };
int output_bit[32] = {185, 179, 170, 155, 152, 149, 143, 5, 137, 122, -1, 110, 107, 104, 98, 2, 95, 89, 86, 83, 74, 71, 56, 191, 50, 44, -1, 38, 29, 14, 8, 188 };
char hex[49];

//                Port  1   2   3   4
char test_vector[NO_TESTS][32] = {
		{'0','0','0','0','0','0','0','X','X','X','X','X','X','X','X','X','L','L','L','L','L','L','L','X','X','X','X','X','X','X','X','X'},
		{'1','0','0','0','0','0','0','X','X','X','X','X','X','X','X','X','H','L','L','L','L','L','L','X','X','X','X','X','X','X','X','X'},
		{'0','1','0','0','0','0','0','X','X','X','X','X','X','X','X','X','L','H','L','L','L','L','L','X','X','X','X','X','X','X','X','X'},
		{'0','0','1','0','0','0','0','X','X','X','X','X','X','X','X','X','L','L','H','L','L','L','L','X','X','X','X','X','X','X','X','X'},
		{'0','0','0','1','0','0','0','X','X','X','X','X','X','X','X','X','L','L','L','H','L','L','L','X','X','X','X','X','X','X','X','X'},
		{'0','0','0','0','1','0','0','X','X','X','X','X','X','X','X','X','L','L','L','L','H','L','L','X','X','X','X','X','X','X','X','X'},
		{'0','0','0','0','0','1','0','X','X','X','X','X','X','X','X','X','L','L','L','L','L','H','L','X','X','X','X','X','X','X','X','X'},
		{'0','0','0','0','0','0','1','X','X','X','X','X','X','X','X','X','L','L','L','L','L','L','H','X','X','X','X','X','X','X','X','X'},
		{'1','1','1','1','1','1','1','X','X','X','X','X','X','X','X','X','H','H','H','H','H','H','H','X','X','X','X','X','X','X','X','X'},
		//{'X','X','X','X','X','X','X','X','X','X','X','X','X','H','X','X','X','X','X','X','X','X','X','X','X','X','X','X','X','X','1','X'}
};


char* bits_to_hex(int* bits)
{
	int i, j, b=191;
	int h;
	for(i=0; i<48; i++) {
		h = 0;
		for(j=3; j>=0; j--)
			if (bits[b--]==1)
					h += pow(2,j);
		sprintf(&hex[i], "%X", h);
	}
	return hex;
}


int main(void)
{
	int i, t, b;

	for(t=0; t<NO_TESTS; t++){
		// Initialise BSR bits to zero
		for(i=0; i<192; i++) {
			BSR_TDI[i]=0;
			BSR_TDO[i]=0;
			BSR_MASK[i]=0;
		}

		// Configure the output pins
		for(b=0; b<32; b++)
			switch(test_vector[t][b]){
			case '0' : case '1' : BSR_TDI[control_bit[b]] = 1;
			}

		// Set the output pins
		for(b=0; b<32; b++)
			switch(test_vector[t][b]){
			case '1' : BSR_TDI[output_bit[b]] = 1;
			}

		// Set the input tests
		for(b=0; b<32; b++)
			switch(test_vector[t][b]){
			case 'L' : BSR_MASK[input_bit[b]] = 1; break;
			case 'H' : BSR_TDO[input_bit[b]] = 1; BSR_MASK[input_bit[b]] = 1; break;
			}



		printf("        TDI(%s);\n", bits_to_hex(BSR_TDI));
		printf("SDR 192 TDO(%s);\n", bits_to_hex(BSR_TDO));
		printf("       MASK(%s);\n", bits_to_hex(BSR_MASK));

	}

//	printf("       TEST(%s);\n", bits_to_hex(BSR_TEST));

	return 0;
}
